<link href="<?php echo base_url("resources/css/pick-a-color-1.2.3.min.css"); ?>" rel="stylesheet">
<script src="<?php echo base_url("resources/javascript/tinycolor-0.9.15.min.js"); ?>"></script>
<script src="<?php echo base_url("resources/javascript/pick-a-color-1.2.3.min.js"); ?>"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$(".pick-a-color").pickAColor({
			showSpectrum            : true,
			showSavedColors         : true,
			saveColorsPerElement    : true,
			fadeMenuToggle          : true,
			showAdvanced			: true,
			showBasicColors         : true,
			showHexInput            : true,
			allowBlank				: false,
			inlineDropdown			: true
		});
	})
</script>