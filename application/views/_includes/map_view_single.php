<?php
	
	$query = $this->db->where('entity_id', $entity_id);
	$query = $this->db->get('entities');
	if ($query->num_rows() == 0)
	{
		die("Cannot find entity.");
	}
?>
<script>
    var map;
    var mapModal;
    $(document).ready(function(){
      map = new GMaps({
        div: '#googleMapSingle',
		zoom: 18,
        lat: <?php echo $entity_latitude; ?>,
        lng: <?php echo $entity_longitude; ?>
      });
      map.addMarker({
        lat: <?php echo $entity_latitude;?>,
        lng: <?php echo $entity_longitude;?>,
        title: '<?php echo $entity_name;?>',
		icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
      });
	  
      mapModal = new GMaps({
        div: '#googleMapSingleModal',
		zoom: 18,
        lat: <?php echo $entity_latitude; ?>,
        lng: <?php echo $entity_longitude; ?>
      });
      mapModal.addMarker({
        lat: <?php echo $entity_latitude;?>,
        lng: <?php echo $entity_longitude;?>,
        title: '<?php echo $entity_name;?>'
      });
			


    });
	$('#mapModal').on('shown.bs.modal', function () {
		$("#googleMapSingleModal").height( '100%' );
		$("#googleMapSingleModal").width( '100%' );
		mapModal.refresh();
		mapModal.setCenter(<?php echo $entity_latitude;?>, <?php echo $entity_longitude;?>);
	});
</script>