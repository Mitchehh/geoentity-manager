<div class="modal fade" id="edit_user" tabindex="-1" role="dialog" aria-labelledby="edit_user" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="edit_user">Edit User</h4>
				<div align="center">User's have access to the system.</div>
			</div>
			<?php $attributes = array('class' => 'form-horizontal', 'role' => 'form')?>
			<?php echo form_open('settings/users/update', $attributes); ?>
				<div class="modal-body">
					<div class="form-group">
						<label for="full_name" class="col-sm-3 control-label">Full Name</label>
						<div class="col-sm-9">
							<?php $attributes = array('id' => 'full_name', 'class' => 'form-control', 'name' => 'full_name', 'placeholder' => 'Bob Down'); ?>
							<?php echo form_input($attributes); ?>
							<p class="help-block">This is the name the user will be referred to throughout the system.</p>
						</div>
					</div>
					<div class="form-group">
						<label for="user_email" class="col-sm-3 control-label">(Login) Email Address</label>
						<div class="col-sm-9">
							<?php $attributes = array('id' => 'user_email', 'class' => 'form-control', 'name' => 'user_email', 'placeholder' => 'bob@googlemail.com'); ?>
							<?php echo form_input($attributes); ?>
							<p class="help-block">Users will use this email to login, recover their password, etc.</p>
						</div>
					</div>
					<div class="form-group">
						<label for="user_password" class="col-sm-3 control-label">Password</label>
						<div class="col-sm-9">
							<?php $attributes = array('class' => 'form-control', 'name' => 'user_password', 'placeholder' => 'Unchanged'); ?>
							<?php echo form_password($attributes); ?>
							<p class="help-block"><em>Leave blank to keep current password.</em></p>
						</div>
					</div>
					<div class="form-group">
						<label for="user_level" class="col-sm-3 control-label">User Access Level</label>
						<div class="col-sm-9">
							<select id="user_level" class="form-control" name="user_level">
								<option value="0">User</option>
								<option value="1">Officer</option>
								<option value="2">Administrator</option>
							</select>
							<p class="help-block">User's can login to the system.<br />
								Officers can make changes to everything except manage users.<br/>
								Administrators have full access and can manage users.</p>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" id="user_id" name="user_id" value="">
					<input type="submit" class="btn-xs btn-danger pull-left" name="action" value="Delete User..." onclick="return confirm('Are you sure you want to delete this user?');">
					<input type="submit" class="btn btn-primary" name="action" value="Save Changes">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close without Saving</button>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
<script>
	$(document).on("click", ".modal_edituser", function () {
		var full_name = $(this).data('fullname');
		$(".modal-body #full_name").val( full_name );
		
		var user_email = $(this).data('useremail');
		$(".modal-body #user_email").val( user_email );
		
		var user_level = $(this).data('userlevel');
		$(".modal-body #user_level").val( user_level );
		
		var user_id = $(this).data('id');
		$(".modal-footer #user_id").val( user_id );
	});
</script>