<div class="modal fade" id="add_user" tabindex="-1" role="dialog" aria-labelledby="add_user" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="add_user">Add New User</h4>
		<div align="center">A user has access to the system, user levels can be set to give special permissions.</div>
      </div>
      <div class="modal-body">
		<?php $attributes = array('class' => 'form-horizontal', 'role' => 'form')?>
		<?php echo form_open('settings/users/add', $attributes); ?>
			<div class="form-group">
				<label for="full_name" class="col-sm-3 control-label">Full Name</label>
				<div class="col-sm-9">
					<?php $attributes = array('class' => 'form-control', 'name' => 'full_name', 'placeholder' => 'Bob Down'); ?>
					<?php echo form_input($attributes); ?>
					<p class="help-block">This is the name the user will be referred to throughout the system.</p>
				</div>
			</div>
			<div class="form-group">
				<label for="user_email" class="col-sm-3 control-label">(Login) Email Address</label>
				<div class="col-sm-9">
					<?php $attributes = array('class' => 'form-control', 'name' => 'user_email', 'placeholder' => 'bob@googlemail.com'); ?>
					<?php echo form_input($attributes); ?>
					<p class="help-block">Users will use this email to login, recover their password, etc.</p>
				</div>
			</div>
			<div class="form-group">
				<label for="user_password" class="col-sm-3 control-label">Password</label>
				<div class="col-sm-9">
					<?php $attributes = array('class' => 'form-control', 'name' => 'user_password', 'placeholder' => '************'); ?>
					<?php echo form_password($attributes); ?>
					<p class="help-block">This is the default password a user will be assigned, they may change this at any time.</p>
				</div>
			</div>
			<div class="form-group">
				<label for="user_level" class="col-sm-3 control-label">User Access Level</label>
				<div class="col-sm-9">
					<select class="form-control" name="user_level">
						<option value="0">User</option>
						<option value="1">Officer</option>
						<option value="2">Administrator</option>
					</select>
					<p class="help-block">User's can login to the system.<br />
						Officers can make changes to everything except manage users.<br/>
						Administrators have full access and can manage users.</p>
				</div>
			</div>
		</div>
			<div class="modal-footer">
				<?php $attributes = array('class' => 'btn btn-primary', 'name' => 'submit', 'value' => 'Add New User'); ?>
				<?php echo form_submit($attributes); ?>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close without Saving</button>
			</div>
			
		<?php echo form_close(); ?>
    </div>
  </div>
</div>