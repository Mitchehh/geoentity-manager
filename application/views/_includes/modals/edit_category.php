<div class="modal fade" id="edit_category" tabindex="-1" role="dialog" aria-labelledby="edit_category" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="edit_category">Edit Category</h4>
      </div>
	<?php $attributes = array('class' => 'form-horizontal', 'role' => 'form')?>
	<?php echo form_open('settings/categories/category/update', $attributes); ?>
      <div class="modal-body">
			<div class="form-group">
				<label for="category_name" class="col-sm-3 control-label">Category Name (singular)</label>
				<div class="col-sm-9">
					<?php $attributes = array('id' => 'category_name', 'class' => 'form-control', 'name' => 'category_name', 'placeholder' => 'Eg. Hydrant / House / Accident Prone Road'); ?>
					<?php echo form_input($attributes); ?>
					<p class="help-block">This is the singular name (no plural) which will be referred to on the map and when assigning entities to an category.</p>
				</div>
			</div>
			<div class="form-group">
				<label for="category_tag" class="col-sm-3 control-label">Tag</label>
				<div class="col-sm-9">
					<?php $attributes = array('id' => 'category_tag', 'class' => 'form-control', 'name' => 'category_tag', 'placeholder' => 'Eg. HYDRANT / HOUSE / ACCIDENT'); ?>
					<?php echo form_input($attributes); ?>
					<p class="help-block">A tag is used as a short name. This is used on maps and entities to provide a quick glympse instead of the full name.</p>
				</div>
			</div>
			<div class="form-group">
				<label for="category_color" class="col-sm-3 control-label">Color</label>
				<div class="col-sm-9">
					<?php $attributes = array('id' => 'category_color', 'class' => 'form-control pick-a-color', 'name' => 'category_color'); ?>
					<?php echo form_input($attributes); ?>
					<p class="help-block">The color is used to for visual aid in separating entities. It is recommended to keep this unique.</p>
				</div>
			</div>
	</div>
			<div class="modal-footer">
					<input type="hidden" id="category_id" name="category_id" value="">
					<input type="submit" class="btn-xs btn-danger pull-left" name="action" value="Delete Category..." onclick="return confirm('Are you sure you want to delete this Category?');">
					<input type="submit" class="btn btn-primary" name="action" value="Save Changes">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close without Saving</button>
				</div>
			
		<?php echo form_close(); ?>
    </div>
  </div>
</div>
<script>
	$(document).on("click", ".modal_editcategory", function () {
		var category_name = $(this).data('name');
		$(".modal-body #category_name").val( category_name );
		
		var category_tag = $(this).data('tag');
		$(".modal-body #category_tag").val( category_tag );
		
		var category_color = $(this).data('color');
		$(".modal-body #category_color").val( category_color );
		
		var category_id = $(this).data('id');
		$(".modal-footer #category_id").val( category_id );
	});
</script>