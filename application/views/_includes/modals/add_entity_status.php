<div class="modal fade" id="add_entity_status" tabindex="-1" role="dialog" aria-labelledby="add_entity_status" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="add_entity_status">Create Entity Status</h4>
	</div>
      <div class="modal-body">
		<?php $attributes = array('class' => 'form-horizontal', 'role' => 'form')?>
		<?php echo form_open('settings/categories/status/add', $attributes); ?>
			<div class="form-group">
				<label for="status_name" class="col-sm-3 control-label">Status Name</label>
				<div class="col-sm-9">
					<?php $attributes = array('class' => 'form-control', 'name' => 'status_name', 'placeholder' => 'Eg. Working / Broken / Unknown'); ?>
					<?php echo form_input($attributes); ?>
					<p class="help-block">.</p>
				</div>
			</div>
			<div class="form-group">
				<label for="status_category" class="col-sm-3 control-label">Assign to Category</label>
				<div class="col-sm-9">
						<?php
							$query = $this->db->get('entity_categories');

							if ($query->num_rows() > 0) {
								echo '<select class="form-control" name="status_category">';
								foreach ($query->result() as $row)
								{
									echo '<option value="'.$row->category_id.'">[' .$row->category_tag. '] ' .$row->category_name. '</option>';
								}
								echo '</select>';
							}
							else {
								echo '<div class="alert alert-danger">You must have at least one category before you can assign an entity status to it!</div>';
							}
						?>
					<p class="help-block">A tag is used as a short name. This is used on maps and entities to provide a quick glympse instead of the full name.</p>
				</div>
			</div>
			<div class="form-group">
				<label for="status_color" class="col-sm-3 control-label">Status Color</label>
				<div class="col-sm-9">
					<?php $attributes = array('class' => 'form-control pick-a-color', 'name' => 'status_color', 'value' => rand(10000, 999999)); ?>
					<?php echo form_input($attributes); ?>
					<p class="help-block">The color is used to for visual aid in separating statuses. It is recommended to have a complimenting color (Eg. Green = Working)</p>
				</div>
			</div>
		</div>
			<div class="modal-footer">
				<?php $attributes = array('class' => 'btn btn-primary', 'name' => 'submit', 'value' => 'Add New Entity Status'); ?>
				<?php echo form_submit($attributes); ?>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close without Saving</button>
			</div>
			
		<?php echo form_close(); ?>
    </div>
  </div>
</div>