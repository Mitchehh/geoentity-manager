<div class="modal fade" id="add_category" tabindex="-1" role="dialog" aria-labelledby="add_category" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="add_category">Create Category</h4>
      </div>
      <div class="modal-body">
		<?php $attributes = array('class' => 'form-horizontal', 'role' => 'form')?>
		<?php echo form_open('settings/categories/category/add', $attributes); ?>
			<div class="form-group">
				<label for="category_name" class="col-sm-3 control-label">Category Name (singular)</label>
				<div class="col-sm-9">
					<?php $attributes = array('class' => 'form-control', 'name' => 'category_name', 'placeholder' => 'Eg. Hydrant / House / Accident Prone Road'); ?>
					<?php echo form_input($attributes); ?>
					<p class="help-block">This is the singular name (no plural) which will be referred to on the map and when assigning entities to an category.</p>
				</div>
			</div>
			<div class="form-group">
				<label for="category_tag" class="col-sm-3 control-label">Tag</label>
				<div class="col-sm-9">
					<?php $attributes = array('class' => 'form-control', 'name' => 'category_tag', 'placeholder' => 'Eg. HYDRANT / SWS / $'); ?>
					<?php echo form_input($attributes); ?>
					<p class="help-block">A tag is used as a short name. This is used on maps and entities to provide a quick glympse instead of the full name.</p>
				</div>
			</div>
			<div class="form-group">
				<label for="category_color" class="col-sm-3 control-label">Color</label>
				<div class="col-sm-9">
					<?php $attributes = array('class' => 'form-control pick-a-color', 'name' => 'category_color', 'value' => rand(10000, 999999)); ?>
					<?php echo form_input($attributes); ?>
					<p class="help-block">The color is used to for visual aid in separating entities. It is recommended to keep this unique.</p>
				</div>
			</div>
		</div>
			<div class="modal-footer">
				<?php $attributes = array('class' => 'btn btn-primary', 'name' => 'submit', 'value' => 'Create Category'); ?>
				<?php echo form_submit($attributes); ?>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close without Saving</button>
			</div>
			
		<?php echo form_close(); ?>
    </div>
  </div>
</div>