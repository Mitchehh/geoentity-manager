<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
	<title><?php echo $page_title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mitch Bevan">
    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link href="<?php echo base_url("resources/css/main.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("resources/css/tipsy.css"); ?>" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url("resources/javascript/jquery.tipsy.js"); ?>"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('.tip').tipsy();
	});
</script>
</head>
<body>