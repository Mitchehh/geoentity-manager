<?php
	$query = $this->db->where('setting', 'map_starting_position_latitude');
	$query = $this->db->get('settings');
	if ($query->num_rows() > 0)
	{
		foreach ($query->result() as $setting)
		{
			$latitude = $setting->value;
		}
	}
	else
	{
		die("Please configure Map Starting Latitude.");
	}
	$query = $this->db->where('setting', 'map_starting_position_longitude');
	$query = $this->db->get('settings');
	if ($query->num_rows() > 0)
	{
		foreach ($query->result() as $setting)
		{
			$longitude = $setting->value;
		}
	}
	else
	{
		die("Please configure Map Starting Longitude.");
	}
?>
<script>
    var map;
		var address;
    $(document).ready(function(){
      map = new GMaps({
        div: '#googleMap',
        lat: <?php echo $latitude; ?>,
        lng: <?php echo $longitude; ?>
      });
		<?php
			//$this->db->where('entity_categories', $category_id);
			$this->db->join('entity_categories', 'entities.entity_category = entity_categories.category_id', 'left');
			$this->db->join('entity_statuses', 'entities.entity_status = entity_statuses.status_id', 'left');
			$query2 = $this->db->get('entities');
			if ($query2->num_rows() > 0)
			{
				foreach ($query2->result() as $entity)
				{
					$entity_id = $entity->entity_id;					
					$entity_category = $entity->entity_category;					
					$entity_name = $entity->entity_name;
					$entity_desc = $entity->entity_desc;
					$entity_latitude = $entity->entity_latitude;
					$entity_longitude = $entity->entity_longitude;
					
					$entity_category_name = $entity->category_name;
					$category_color = $entity->category_color;
					
					$entity_status_name = $entity->status_name;
					$entity_status_color = $entity->status_color;
					
					if (!$entity_category_name) $entity_category_name = "No Category Defined";
		?>
		test = new GMaps.geocode({
			lat: <?php echo $entity_latitude;?>,
			lng: <?php echo $entity_longitude;?>,
			  callback: function(results, status) {
				if (status == 'OK') {
				  address = results[0].formatted_address;
				}
			}
		});
		console.log(address);
		
		
      map.addMarker({
        lat: <?php echo $entity_latitude;?>,
        lng: <?php echo $entity_longitude;?>,
        title: '<?php echo $entity_name;?>',
        infoWindow: {
          content: '<h5><span class="label label-default" style="background-color:#<?php echo $category_color; ?>"><?php echo $entity_category_name;?></span>&nbsp;<span class="label label-default" style="background-color: #<?php echo $entity_status_color; ?>"><?php echo $entity_status_name; ?></span></h5> <h3><?php echo $entity_name;?></h3><?php if ($entity_desc) echo $entity_desc; ?><hr>Estimated Location: '+address+'<div align="center"><a href="<?php echo site_url('entity/view/'.$entity_id); ?>" class="btn btn-info">View More Information</a></div>'
        }
      });
	<?php
				}
			}
	?>
		map.setContextMenu({
			control: 'map',
			options: [{
				title: 'Add marker',
				name: 'add_marker',
				action: function(e) {
					window.location.href = "<?php echo site_url('entity/add/')?>/"+e.latLng.lat()+"/"+e.latLng.lng();
					this.addMarker({
						lat: e.latLng.lat(),						
						lng: e.latLng.lng(),
						title: 'Adding New Marker Here...'
					});
				}
			}, {
				title: 'Center here',
				name: 'center_here',
				action: function(e) {
					this.setCenter(e.latLng.lat(), e.latLng.lng());
				}
			}]
		});
    });
	
	$(document).on('click', '.pan-to-marker', function(e) {
		e.preventDefault();
		var lat, lng;
		var $lat = $(this).data('marker-lat');
		var $lng = $(this).data('marker-lng');
		lat = $lat;
		lng = $lng;
		map.setCenter(lat, lng);
	});
	</script>