<?php
	$site_name = "Location Manager";
	$user_name = $this->session->userdata('user_name');
	$full_name = $this->session->userdata('full_name');
	$level = $this->session->userdata('level');
	$last_logged_in = $this->session->userdata('last_logged_in');
?>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url(); ?>"><?php echo $site_name; ?></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Overview <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li>
							<a href="<?php echo site_url("overview/map");?>"><span class="glyphicon glyphicon-screenshot"></span>&nbsp;&nbsp; Map Overview</a>
						</li>
						<li>
							<a href="<?php echo site_url("overview/entities");?>"><span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp; List Overview</a>
						</li>
					</ul>
				</li>
			</ul>
		  
			<ul class="nav navbar-nav">
				<li>
				<a href="<?php echo site_url("entity/add");?>"><span class="glyphicon glyphicon-plus-sign"></span> New Entity</a>
				</li>
			</ul>
		  
          <ul class="nav navbar-nav navbar-right">
		  <?php if ($level > 0) { ?>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-wrench"></span> Administration <b class="caret"></b></a>
              <ul class="dropdown-menu">
			  
				<?php if ($level > 1) { ?>
                <li>
					<div align="center"><strong>Administrator Settings</strong></div>
                </li>
                <li>
					<a href="<?php echo site_url("settings/site"); ?>"><span class="glyphicon glyphicon-cog"></span>&nbsp;&nbsp; Site Settings</a>
                </li>
                <li>
					<a href="<?php echo site_url("settings/users"); ?>"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp; User Management</a>
                </li>
                <li>
					<a href="<?php echo site_url("settings/logs"); ?>"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp; Logs</a>
                </li>
                <li class="divider"></li>
				<?php } ?>
                <li>
					<div align="center"><strong>Officer Settings</strong></div>
                </li>
                <li>
					<a href="<?php echo site_url("settings/categories"); ?>"><span class="glyphicon glyphicon-th-large"></span>&nbsp;&nbsp; Categories &amp; Statuses Management</a>
                </li>
                <li>
					<a href="<?php echo site_url("settings/entities"); ?>"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;&nbsp; Entity Management</a>
                </li>
              </ul>
            </li>
			<?php } ?>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $full_name; ?><b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li>
					<a href="<?php echo site_url("profile"); ?>"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp; View My Profile</a>
                </li>
				<li class="divider"></li>
				<?php if($level > 0) { ?>
					<li>
						<a><strong>Role:</strong> <?php if ($level == 1) { echo "Moderator";} elseif ($level == 2) { echo "Administrator"; }?></a>
					</li>
				<?php } ?>
                <li class="divider"></li>
                <li>
					<a href="<?php echo site_url("logout"); ?>"><span class="glyphicon glyphicon-off"></span>&nbsp;&nbsp;Logout</a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
      </div>
      <!-- /.container-fluid -->
    </nav>