<?php
	$this->db->where('user_id', $user_id);
	$query = $this->db->get('users');

	if ($query->num_rows() == 1) {
		foreach ($query->result() as $row)
		{
			$user_id = $row->user_id;
			$full_name = $row->full_name;
			$user_level = $row->user_level;
			$user_registered = $row->user_registered;
			$user_last_login = $row->user_last_login;
		}
	}
?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?php
				$success_message = $this->session->flashdata('success_message');
				$error_message = $this->session->flashdata('error_message');
				
				if ($success_message)
				{
					echo '<div class="alert alert-success">'.$success_message.'</div>';
					echo '<hr>';
				}
				if ($error_message or validation_errors())
				{
					echo '<div class="alert alert-danger">'.$error_message.'</div>';
					echo '<hr>';
				}
			?>
			<h1><?php echo $full_name;?>'s Profile</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			
			<ul class="nav nav-tabs" id="profileTab">
				<li class="active"><a href="#info" data-toggle="tab"><span class="glyphicon glyphicon-info-sign"></span> Information</a></li>
				<li><a href="#comments" data-toggle="tab"><span class="glyphicon glyphicon-comment"></span> Recent Comments</a></li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="info" style="padding:10px;">
					<div class="row">
						<div class="col-md-12">
							<p><strong>User Registered:</strong> <abbr class="tip" original-title="<?php echo standard_date('DATE_RFC1123', strtotime($user_registered)); ?>"><?php echo timespan(strtotime($user_registered), time()) . ' ago'; ?></abbr></p>
							<p><strong>Last Logged In:</strong> <abbr class="tip" original-title="<?php echo standard_date('DATE_RFC1123', strtotime($user_last_login)); ?>"><?php echo timespan(strtotime($user_last_login), time()) . ' ago'; ?></abbr></p>
							<hr>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="comments" style="padding:10px;">
					<div class="row">
						<div class="col-md-12">
							<?php
								$this->db->where('comment_by', $user_id);
								$this->db->join('entities', 'entity_comments.entity_id = entities.entity_id');
								$this->db->order_by('comment_id', 'desc');
								$this->db->limit(10);
								$query = $this->db->get('entity_comments');
								
								if ($query->num_rows() > 0) {
									foreach ($query->result() as $row)
									{
										$comment = $row->comment;
										$comment_time = $row->comment_time;
										$entity_id = $row->entity_id;
										$entity_name = $row->entity_name;
										?>
										<p>
											<strong><?php echo $full_name;?></strong> <small><abbr class="tip" original-title="<?php echo standard_date('DATE_RFC1123', strtotime($comment_time)); ?>"><?php echo timespan(strtotime($comment_time), time()) . ' ago'; ?></abbr> on <?php echo anchor('entity/view/'.$entity_id, $entity_name); ?></small><br />
											<?php echo $comment; ?>
										</p>
										<hr>
										<?php
									}
								}
								else {
									echo '<div class="alert alert-info">'.$full_name.' hasn\'t left any comments on any entities.<br />Maybe encourage them too?</div>';
								}
							
							?>
				</div>
			</div>
		</div>
	</div>
</div>
