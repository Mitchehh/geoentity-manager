<div class="main-map">
	<div class="row">
		<div class="col-md-3">
          <div class="well">
            <h3>
              Entities List
            </h3>
            <div>
              Welcome to the Entities List screen.&nbsp;
              <span style="font-size: 14px; line-height: 1.428571429;">Here you can view all your entities in a list, filter entities by category and search entities. You can also manage entities easily through this screen.</span>
            </div>
          </div>
          <div class="form-group">
            <select class="form-control" name="entity_category">
                Filter Entities by Category
				<option value="0">All Categories</option>
				<?php
					$searchquery = $this->db->get('entity_categories');
					if ($searchquery->num_rows() > 0)
					{
						foreach ($searchquery->result() as $searchrow)
						{
							$category_id = $searchrow->category_id;
							$category_name = $searchrow->category_name;
							$category_tag = $searchrow->category_tag;
							$category_color = $searchrow->category_color;
							echo '<option value="'.$category_id.'">['.$category_tag.'] '.$category_name.'</option>';
						}
					}
				?>
			</select>
          </div>
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Filter Entities by Text">
          </div>
        </div>
        <div class="col-md-9">
          <h3>
            List View of All Entities by Category
          </h3>
			<div class="panel-group" id="accordion">
				<?php
					$query = $this->db->get('entity_categories');
					if ($query->num_rows() > 0)
					{

						foreach ($query->result() as $row)
						{
							$category_id = $row->category_id;
							$category_name = $row->category_name;
							$category_tag = $row->category_tag;
							$category_color = $row->category_color;
							
							$this->db->where('entities.entity_category', $category_id);
							$query1 = $this->db->get('entities');
							if ($query1->num_rows() > 0)
							{
							?>
							
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $category_id; ?>">
											<span class="label label-default" style="color:#<?php echo $category_color; ?>; background-color:#FFF;"><?php echo $category_tag; ?></span>
											<?php echo plural($category_name); ?>
											<span class="pull-right">&nbsp;</span>
											<?php
												$this->db->where('entity_category_id', $category_id);
												$query2 = $this->db->get('entity_statuses');
												if ($query2->num_rows() > 0)
												{
													foreach ($query2->result() as $row)
													{
													
														$this->db->where('entity_status', $row->status_id);
														$query3 = $this->db->get('entities');
														$value = $query3->num_rows();
											?>
												<span class="label pull-right label-default" style="background-color:#<?php echo $row->status_color; ?>;"><?php echo $value . " " .$row->status_name; ?></span><span class="pull-right">&nbsp;</span>
											<?php
													}
												}
											?>
										</a>
									</h4>
								</div>
								<div id="collapse<?php echo $category_id; ?>" class="panel-collapse collapse" style="height: auto;">
									<div class="panel-body">
												<?php 
													$this->db->where('entities.entity_category', $category_id);
													$this->db->join('users', 'users.user_id = entities.entity_last_updated_by');
													$query4 = $this->db->get('entities');
													if ($query4->num_rows() > 0)
													{
												?>
														<table class="table">
															<thead>
															  <tr>
																<th>
																  Name
																  <span class="glyphicon glyphicon-sort-by-attributes" title="To Do..."></span>
																</th>
																<th>
																  Last Updated
																  <span class="glyphicon glyphicon-sort-by-attributes" title="To Do..."></span>
																</th>
																<th>
																  Options (Edit, Delete, Status)
																  <span class="glyphicon glyphicon-sort-by-attributes" title="To Do..."></span>
																</th>
															  </tr>
															 </thead>
															 <tbody>
																<?php
																	foreach ($query4->result() as $entity)
																	{
																		$entity_id = $entity->entity_id;
																		$entity_category = $entity->entity_category;
																		$entity_name = $entity->entity_name;
																		$entity_latitude = $entity->entity_latitude;
																		$entity_longitude = $entity->entity_longitude;
																		$entity_last_updated_by = $entity->entity_last_updated_by;
																		$entity_last_updated_by_name = $entity->full_name;
																		$entity_last_updated_time = $entity->entity_last_updated_time;
																?>
																		<tr>
																			<td>
																			  <strong><?php echo anchor('entity/view/'.$entity_id, $entity_name); ?></strong>
																			</td>
																			<td>
																			  <abbr class="tip" original-title="<?php echo standard_date('DATE_RFC1123', strtotime($entity_last_updated_time)); ?>"><?php echo timespan(strtotime($entity_last_updated_time), time()) . ' ago'; ?></abbr> by <?php echo anchor("profile/view/".$entity_last_updated_by, $entity_last_updated_by_name); ?>
																			</td>
																			<td>
																			  <div class="btn-group">
																				<a href="#" class="btn btn-default" title="Edit Entity..."><span class="glyphicon glyphicon-pencil"></span></a>
																				<a href="#" class="btn btn-default" title="Delete Entity..."><span class="glyphicon glyphicon-remove-circle"></span></a>
																				<div class="btn-group">
																				  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
																					Update Status
																					<span class="caret"></span>
																				  </button>
																				  <ul class="dropdown-menu" role="menu">
																					<li>
																					  <a href="#"><span class="label pull-right label-success">WORKING</span></a>
																					</li>
																					<li>
																					  <a href="#"><span class="label pull-right label-danger">BROKEN</span></a>
																					</li>
																				  </ul>
																				</div>
																			  </div>
																			</td>
																		  </tr>
															<?php
																	} 
																?>
															</tbody>
														</table>
													<?php
														}
														else
														{
															echo '<div class="alert alert-danger"><strong>No one here but us chickens.</div>';
														}
													?>
								</div>
							</div>
						</div>
						<?php
							}
						}
					}
				?>
			</div>         
          </div>
        </div>
      </div>