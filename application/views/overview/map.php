
    <div class="main-map">
      <div class="row">
        <div class="col-md-4">
          <div class="well" align="center">
            <div>
              <h3 style="margin:0px;">
                Map View
              </h3>
            </div>
          </div>
			<hr>
          <div class="form-group">
            <label>
              Filter Options
            </label>
            <input type="text" class="form-control" placeholder="Filter by Text">
          </div>
          <hr>
          <div>
			<div class="panel-group" id="accordion">
				<?php
					$query = $this->db->get('entity_categories');
					if ($query->num_rows() > 0)
					{
						foreach ($query->result() as $row)
						{
							$category_id = $row->category_id;
							$category_name = $row->category_name;
							$category_tag = $row->category_tag;
							$category_color = $row->category_color;
							
							$this->db->join('entity_statuses', 'entities.entity_status = entity_statuses.status_id', 'left');
							$this->db->where('entities.entity_category', $category_id);
							$query1 = $this->db->get('entities');
							if ($query1->num_rows() > 0)
							{
							?>
							
							<div class="panel panel-default">
								<div class="panel-heading" style="color: #FFF; background-color:#<?php echo $category_color; ?>;">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $category_id; ?>">
											<?php echo plural($category_name); ?>
											<span class="label pull-right label-default" style="color:#<?php echo $category_color; ?>; background-color:#FFF;"><?php echo $category_tag; ?></span>
										</a>
									</h4>
								</div>
								<div id="collapse<?php echo $category_id; ?>" class="panel-collapse collapse" style="height: auto;">
									<div class="panel-body">
									<?php 
										$this->db->where('entity_category', $category_id);
										$query2 = $this->db->get('entities');
										if ($query2->num_rows() > 0)
										{
											foreach ($query2->result() as $entity)
											{
												$entity_id = $entity->entity_id;
												$entity_category = $entity->entity_category;
												$entity_name = $entity->entity_name;
												$entity_desc = $entity->entity_desc;
												$entity_latitude = $entity->entity_latitude;
												$entity_longitude = $entity->entity_longitude;
												$entity_last_viewed_by = $entity->entity_last_viewed_by ?: FALSE;
												$entity_last_viewed_time = $entity->entity_last_viewed_time ?: FALSE;
												$entity_last_updated_by = $entity->entity_last_updated_by;
												$entity_last_updated_time = $entity->entity_last_updated_time;
												
												$this->db->where('status_id', $entity->entity_status);
												$query11 = $this->db->get('entity_statuses');
												if ($query11->num_rows() == 1)
												{
													foreach ($query11->result() as $entity_status)
													{
														$entity_status_category_id = $entity_status->entity_category_id;
														$entity_status_name = $entity_status->status_name;
														$entity_status_color = $entity_status->status_color;
													}
												}
												else {
													$entity_status_category_id = -1;
													$entity_status_name = "";
													$entity_status_color = "transparent";
												}
												
												if ($entity_last_viewed_by) {
													$this->db->where('user_id', $entity->entity_last_viewed_by);
													$query3 = $this->db->get('users');
													if ($query3->num_rows() == 1)
													{
														foreach ($query3->result() as $user)
														{
															$entity_last_viewed_by_name = $user->full_name;
														}
													}
												}
												else
												{
													$entity_last_viewed_time = "Never";
													$entity_last_viewed_by_name = "No One";
												}
												
												$this->db->where('user_id', $entity->entity_last_updated_by);
												$query4 = $this->db->get('users');
												if ($query4->num_rows() == 1)
												{
													foreach ($query4->result() as $user2)
													{
														$entity_last_updated_by_name = $user2->full_name;
													}
												}
									?>
												<div>
													<h4><span class="label label-default pull-right" style="background-color: #<?php echo $entity_status_color; ?>"><?php echo $entity_status_name; ?></span></h4>
													<h3><?php echo $entity_name; ?></h3>
													<?php if($entity_desc) echo "<br />".$entity_desc; ?><hr>
													<p>Last Viewed By <?php echo anchor('profile/view/'.$entity_last_viewed_by, $entity_last_viewed_by_name); ?> <small><abbr class="tip" original-title="<?php echo standard_date('DATE_RFC1123', strtotime($entity_last_viewed_time)); ?>"><?php echo timespan(strtotime($entity_last_viewed_time), time()) . ' ago'; ?></abbr></small></p>
													<p>Last Updated By <?php echo anchor('profile/view/'.$entity_last_updated_by, $entity_last_updated_by_name); ?> <small><abbr class="tip" original-title="<?php echo standard_date('DATE_RFC1123', strtotime($entity_last_updated_time));?>"><?php echo timespan(strtotime($entity_last_updated_time), time()) . ' ago'; ?></abbr></small></p>
													<a href="#" class="btn btn-success pan-to-marker" data-marker-lat="<?php echo $entity_latitude; ?>" data-marker-lng="<?php echo $entity_longitude; ?>">Centre on Map</a>
													<a href="<?php echo site_url('entity/view/'.$entity_id); ?>" class="btn btn-info">View More Information</a>
												</div>
												<br />
												<hr>
									<?php
											} 
										}
										else
										{
										
										}
									?>
									<div align="center"><strong>END OF LIST</strong> Total: <?php echo $query2->num_rows(); ?></div>
								</div>
							</div>
						</div>
						<?php
							}
						}
					}
				?>
             
                </div>
              </div>
		</div>
        <div class="col-md-8">
          <div id="googleMap"></div>
        </div>
      </div>
    </div>	