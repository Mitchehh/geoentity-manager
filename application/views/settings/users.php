<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1><?php echo $page_title;?></h1>
		</div>
	</div>
	<?php // form_open('settings/users'); ?>
	<div class="row">
		<div class="col-md-12">
		
			  <a href="#" class="btn btn-success pull-right" data-toggle="modal" data-target="#add_user"><span class="glyphicon glyphicon-plus-sign"></span> Add New User...</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php
				$success_message = $this->session->flashdata('success_message');
				$error_message = $this->session->flashdata('error_message');
				
				if ($success_message)
				{
					echo '<div class="alert alert-success">'.$success_message.'</div>';
					echo '<hr >';
				}
				if ($error_message or validation_errors())
				{
					echo '<div class="alert alert-danger"><strong>Sorry... an error occured:</strong><br />'.$error_message . validation_errors() .'</div>';
					echo '<hr >';
				}
				
				$query = $this->db->get('users');

				if ($query->num_rows() > 0) {
					echo '<table class="table table-hover">';
						echo '<thead>';
							echo '<tr>';
								echo '<th>Full Name</th>';
								echo '<th>Email Address</th>';
								echo '<th>User Level</th>';
								echo '<th>Registered</th>';
								echo '<th>Last Logged In</th>';
								echo '<th>Logins</th>';
								echo '<th>Modify</th>';
							echo '</tr>';
						echo '</thead>';
					echo '<tbody>';
					foreach ($query->result() as $row)
					{
						
						if ($row->user_level == 1) {
							$level = "Officer";
						}
						elseif ($row->user_level == 2) {
							$level = "Administrator";
						}
						else {
							$level = "User";
						}
						echo '<tr>';
							echo '<td>' .anchor('profile/view/'.$row->user_id, $row->full_name). '</td>';
							echo '<td>' .anchor("mailto://".$row->user_email, $row->user_email). '</td>';
							echo '<td>' .$level. '</td>';
							echo '<td><abbr class="tip" original-title="' .standard_date('DATE_RFC1123', strtotime($row->user_registered)) .'">'.timespan(strtotime($row->user_registered), time()) . ' ago'.'</abbr></td>';
							if ($row->user_last_login) {
								echo '<td><abbr class="tip" original-title="' .standard_date('DATE_RFC1123', strtotime($row->user_last_login)) .'">'.timespan(strtotime($row->user_last_login), time()) . ' ago'.'</abbr></td>';
							}
							else
							{
								echo '<td>Never</td>';
							}
							echo '<td>'.$row->user_logins.'</td>';
							echo '<td><a href="#" class="btn-sm btn-warning pull-right modal_edituser" data-toggle="modal" data-id="'.$row->user_id.'"  data-fullname="'.$row->full_name.'" data-useremail="'.$row->user_email.'" data-userlevel="'.$row->user_level.'"  data-target="#edit_user"><span class="glyphicon glyphicon-pencil"></span> </a></td>';
						echo '</tr>';
    				}
					echo '</tbody>';
					echo '</table>';
				}
			?>

		</div>
	</div>
</div>