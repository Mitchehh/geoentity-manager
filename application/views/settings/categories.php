<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1><?php echo $page_title;?></h1>
			<hr>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<?php
			$success_message = $this->session->flashdata('success_message');
			$error_message = $this->session->flashdata('error_message');
				
			if ($success_message)
			{
				echo '<div class="alert alert-success">'.$success_message.'</div>';
				echo '<hr >';
			}
			if ($error_message or validation_errors())
			{
				echo '<div class="alert alert-danger"><strong>Sorry... an error occured:</strong><br />'.$error_message . validation_errors() .'</div>';
				echo '<hr >';
			}
		?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
		<h2>Manage Categories</h2>
		<p><a href="#" class="btn-sm btn-success pull-right" data-toggle="modal" data-target="#add_category"><span class="glyphicon glyphicon-plus-sign"></span> Create Category...</a></p>
		<div align="center">An category is a group which stores entities, a category required before creating an entity.</div>
			<?php				
				$this->db->order_by("entity_categories.category_name", "ASC"); 
				$query = $this->db->get('entity_categories');

				if ($query->num_rows() > 0) {
					echo '<table class="table table-hover">';
						echo '<thead>';
							echo '<tr>';
								echo '<th>Category Name</th>';
								echo '<th>Category Tag</th>';
								echo '<th>Category Color</th>';
								echo '<th>Manage</th>';
							echo '</tr>';
						echo '</thead>';
					echo '<tbody>';
					foreach ($query->result() as $row)
					{
						echo '<tr>';
							echo '<td>' .$row->category_name. '</td>';
							echo '<td>' .$row->category_tag. '</td>';
							echo '<td><div style="display:inline-block; background-color:#'.$row->category_color.'; -moz-border-radius: 15px; -webkit-border-radius: 15px; border-radius: 15px; border:solid 1px #CCC; width:15px; height:15px;"></div> #' .$row->category_color . '</td>';
							echo '<td><a href="#" class="btn-sm btn-warning pull-right modal_editcategory" data-toggle="modal" data-id="'.$row->category_id.'"  data-name="'.$row->category_name.'" data-tag="'.$row->category_tag.'" data-color="'.$row->category_color.'" data-target="#edit_category"><span class="glyphicon glyphicon-pencil"></span></a></td>';
						echo '</tr>';
    				}
					echo '</tbody>';
					echo '</table>';
				}
				else
				{
					echo '<div class="alert alert-danger"><strong>You have no category!</strong><br />You need to create at least 1 category before you can use this system.<br />Create a category by clicking "Create Category" at the top right of this message.</div>';
				}
			?>
		</div>
		<div class="col-md-6">
		<h2>Manage Entity Statuses</h2>
		<p><a href="#" class="btn-sm btn-success pull-right" data-toggle="modal" data-target="#add_entity_status"><span class="glyphicon glyphicon-plus-sign"></span> Create Entity Status...</a></p>
		<div align="center">An entity status is assigned to a group. This allows "statuses" to be set on entities. Eg. Working/Broken.</div>
			<?php				
				$this->db->join('entity_categories', 'entity_categories.category_id = entity_statuses.entity_category_id');
				$this->db->order_by("entity_categories.category_name", "ASC"); 
				$query = $this->db->get('entity_statuses');

				if ($query->num_rows() > 0) {
					echo '<table class="table table-hover">';
						echo '<thead>';
							echo '<tr>';
								echo '<th>Category Assigned</th>';
								echo '<th>Status Name</th>';
								echo '<th>Status Color</th>';
								echo '<th>Manage</th>';
							echo '</tr>';
						echo '</thead>';
					echo '<tbody>';
					foreach ($query->result() as $row)
					{
						echo '<tr>';
							echo '<td>' .$row->category_name. '</td>';
							echo '<td>' .$row->status_name. '</td>';
							echo '<td><div style="display:inline-block; background-color:#'.$row->status_color.'; -moz-border-radius: 15px; -webkit-border-radius: 15px; border-radius: 15px; border:solid 1px #CCC; width:15px; height:15px;"></div> #' .$row->status_color . '</td>';
							echo '<td><a href="#" class="btn-sm btn-warning pull-right modal_editstatus" data-toggle="modal" data-id="'.$row->status_id.'"  data-name="'.$row->status_name.'" data-category="'.$row->entity_category_id.'" data-color="'.$row->status_color.'" data-target="#edit_entity_status"><span class="glyphicon glyphicon-pencil"></span></a></td>';
						echo '</tr>';
    				}
					echo '</tbody>';
					echo '</table>';
				}
				else
				{
					echo '<div class="alert alert-danger"><strong>You have no entity statuses!</strong><br />Create a new entity status by clicking "Create Entity Status" at the top right of this message.</div>';
				}
			?>
		</div>
	</div>
</div>