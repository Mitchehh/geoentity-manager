<div class="mainmap" style="padding:10px;">
	<div class="row">
		<div class="col-md-12">
			<h1><?php echo $page_title;?></h1>
			<hr>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<?php
			$success_message = $this->session->flashdata('success_message');
			$error_message = $this->session->flashdata('error_message');
				
			if ($success_message)
			{
				echo '<div class="alert alert-success">'.$success_message.'</div>';
				echo '<hr >';
			}
			if ($error_message or validation_errors())
			{
				echo '<div class="alert alert-danger"><strong>Sorry... an error occured:</strong><br />'.$error_message . validation_errors() .'</div>';
				echo '<hr >';
			}
		?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table">
			  <thead>
				<tr>
				  <th>Timestamp</th>
				  <th>Success/Fail</th>
				  <th>Message</th>
				  <th>User</th>
				  <th>IP Address</th>
				</tr>
			  </thead>
			  <tbody>
					<?php
					$this->db->join('users', 'logs.log_initiated_by = users.user_id', 'LEFT');
					$this->db->order_by('log_id', 'DESC');
					$logs = $this->db->get('logs');
					if ($logs->num_rows() > 0)
					{
						foreach ($logs->result() as $log)
						{
							$log_id = $log->log_id;
							$time = $log->log_time;
							$success = $log->log_success;
							if ($success) {
								$success_m = "SUCCESS";
							} elseif (!$success) {
								$success_m = "FAILURE";
							}
							
							$message = $log->log_message;
							$initiated_by = $log->log_initiated_by;
							$ipaddress = $log->log_ipaddress;
							if ($initiated_by == -1) {
								$user = "System";
							}
							else {
								$username = $log->full_name;
								$user = anchor("profile/view/".$initiated_by, $username);
								
								if (!$username) {
									$user = "Unknown";
								}									
							}
						?>
						<tr <?php if ($success) { echo 'class="success"'; } else { echo 'class="danger"'; } ?>>
							<td><?php echo $time; ?></td>
							<td><?php echo $success_m; ?></td>
							<td><?php echo $message; ?></td>
							<td><?php echo $user; ?></td>
							<td><?php echo anchor("http://whatismyipaddress.com/ip/".$ipaddress, $ipaddress, array('target' => '_blank')); ?></td>
						</tr>
						<?php
						}
					}
					?>
							
			  </tbody>
			</table>
		</div>
	</div>
</div>