<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1><?php echo $page_title;?></h1>
			<p><strong>All fields below are required</strong>!</p>
			<hr>
		</div>
	</div>
	<?php $attributes = array('class' => 'form-horizontal', 'role' => 'form')?>
	<?php echo form_open('settings/site'); ?>
	<div class="row">
		<div class="col-md-12">
			<h3>Global</h3>
			<div class="form-group">
				<label for="site_name" class="col-sm-2 control-label">Organisation Name</label>
				<div class="col-sm-10">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span>
						<?php $attributes = array('id' => 'site_name', 'class' => 'form-control', 'name' => 'site_name', 'placeholder' => 'Eg. Hillbilly Fire Brigade'); ?>
						<?php echo form_input($attributes); ?>
					</div>
					<p class="help-block">This is your organisation's name for reference throughout the site.</p>
				</div>
			</div>
			<div class="form-group">
				<label for="site_name" class="col-sm-2 control-label">System Name</label>
				<div class="col-sm-10">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-leaf"></span></span>
						<?php $attributes = array('id' => 'site_name', 'class' => 'form-control', 'name' => 'site_name', 'placeholder' => 'Eg. Hydrant Manager / Entity Manager / Location Database'); ?>
						<?php echo form_input($attributes); ?>
					</div>
					<p class="help-block">This is the name of what this whole site is, it is customisable as you may wish to use it's functionality to suit your needs.</p>
				</div>
			</div>
			<h3>Google Maps</h3>
			<div class="form-group">
				<label for="site_name" class="col-sm-2 control-label">Browser API Key</label>
				<div class="col-sm-10">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
						<?php $attributes = array('id' => 'site_name', 'class' => 'form-control', 'name' => 'site_name', 'placeholder' => 'Eg. BIzaSyAr50qkWfWNQsM0Ou1s7pr129rkggkflmM'); ?>
						<?php echo form_input($attributes); ?>
					</div>
					<p class="help-block">This is required, please enter the Browser API Key you get from <a href="https://developers.google.com/maps/documentation/javascript/tutorial#api_key" target="_blank">Google</a> and enter it here.</p>
				</div>
			</div>
			<div class="form-group">
				<label for="site_name" class="col-sm-2 control-label">Map Starting - Location</label>
				<div class="col-sm-10">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-pushpin"></span></span>
						<?php $attributes = array('id' => 'site_name', 'class' => 'form-control', 'name' => 'site_name', 'placeholder' => 'Eg. Forest Hill, NSW, 2651, Australia'); ?>
						<?php echo form_input($attributes); ?>
					</div>
					<p class="help-block">Please enter in a full address or town. This will be the starting position/view of your maps.<br />This will be converted to Latitude and Longitude.</p>
				</div>
			</div>
			<div class="form-group">
				<label for="site_name" class="col-sm-2 control-label">Map Starting - Zoom Level</label>
				<div class="col-sm-10">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-record"></span></span>
						<select name="map_zoom_level" class="form-control">
							<?php
								$attributes = array('id' => 'site_name', 'class' => 'form-control', 'name' => 'site_name');
								for ($x = 0; $x <= 21; $x++)
								{
									if ($x == 0) {
										echo '<option value="'.$x.'">'.$x.' (Very Far Away)</option>';
									}
									elseif ($x == 21) {
										echo '<option value="'.$x.'">'.$x.' (Very Close)</option>';
									}
									elseif ($x == 13) {
										echo '<option value="'.$x.'">'.$x.' (Recommended)</option>';
									}
									else {
										echo '<option value="'.$x.'">'.$x.'</option>';
									}
								}
							?>
						</select>
					</div>
					<p class="help-block">This is used with the map starting view.</p>
				</div>
			</div>
			<h3>Miscellaneous</h3>
			<div class="form-group">
				<label for="site_name" class="col-sm-2 control-label">Registration Contact</label>
				<div class="col-sm-10">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
						<?php $attributes = array('id' => 'site_name', 'class' => 'form-control', 'name' => 'site_name', 'placeholder' => 'Eg. your Brigade Captain or Hydrant Manager Administrator'); ?>
						<?php echo form_input($attributes); ?>
					</div>
					<p class="help-block">Displayed on the login page. Who to contact if you should need an account..</p>
				</div>
			</div>
		</div>
	</div>
</div>