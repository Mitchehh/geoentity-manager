          <div class="inner cover">
            <h1 class="cover-heading">Welcome to Happiness.io</h1>
            <p class="lead">We're new here, like you. But our goal is to make it simple for your customers to give you feedback on how they feel.<br />So give us a try, it's free. :)</p>
            <p class="lead">
              <a href="<?php echo site_url('register'); ?>" class="btn btn-lg btn-default">Register for a Free Account</a>
            </p>
          </div>

        </div>

      </div>

    </div>
