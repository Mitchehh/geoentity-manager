
	<div class="inner cover">
		<h1 class="cover-heading">Register a New Account.</h1>

		<p>We're so glad you've taken an interest in registering!</p>
		<p>Fill out the info below and we'll get started shortly...</p>
		<hr>
		<?php
			$error_message = $this->session->flashdata('error_message');

			if ($error_message or validation_errors()) {
				echo '<div class="alert alert-danger"><strong>Sorry... an error occured:</strong><br />'.$error_message . validation_errors() .'</div>';
				echo '<hr >';
			}
		?>

		<?php echo form_open('register/'); ?>
	
  		<div class="form-group">
  			<?php $attributes = array('class' => 'col-sm-3 control-label'); ?>
			<?php echo form_label('Your Name', 'name', $attributes); ?>

    		<div class="col-sm-9">
				<?php $data = array('class' => 'form-control', 'name' => 'name', 'placeholder' => 'Joe Walker'); ?>
				<?php echo form_input($data); ?>
			<p class="help-block">This is the name we'll call you when you use Smiley Face.</p>
			</div>
		</div>


  		<div class="form-group">			
  			<?php $attributes = array('class' => 'col-sm-3 control-label'); ?>
			<?php echo form_label('Email Address', 'email', $attributes); ?>

    		<div class="col-sm-9">
				<?php $data = array('class' => 'form-control', 'name' => 'email', 'placeholder' => 'email@address.com'); ?>
				<?php echo form_input($data); ?>
				<p class="help-block">This is your login, where we'll send reports and password resets.</p>
			</div>
		</div>
			

  		<div class="form-group">
  			<?php $attributes = array('class' => 'col-sm-3 control-label'); ?>
			<?php echo form_label('Password', 'password', $attributes); ?>
    		<div class="col-sm-9">
				<?php $data = array('class' => 'form-control', 'name' => 'password'); ?>
				<?php echo form_password($data); ?>
			<p class="help-block">This is your password, make it bloody secure.</p>
			</div>
		</div>

  		<div class="form-group">
  			<?php $attributes = array('class' => 'col-sm-3 control-label'); ?>
			<?php echo form_label('Confirm Password', 'passconf', $attributes); ?>

    		<div class="col-sm-9">
				<?php $data = array('class' => 'form-control', 'name' => 'passconf'); ?>
				<?php echo form_password($data); ?>
			<p class="help-block">Confirm your bloody secure password.</p>
			</div>
		</div>

  		<div class="form-group">
  			<?php $attributes = array('class' => 'col-sm-3 control-label'); ?>
			<?php echo form_label('Registration Code', 'code', $attributes); ?>

    		<div class="col-sm-9">
				<?php $data = array('class' => 'form-control', 'name' => 'code'); ?>
				<?php echo form_password($data); ?>
			<p class="help-block">This service is in a closed beta. Please enter the entry code.</p>
			</div>
		</div>
		

  		<div class="form-group">
  			<?php $attributes = array('class' => 'control-label'); ?>
			<?php echo form_label('Do you agree to our <a href="' . site_url("tos/") . '" target="_blank">Terms of Service</a>?', 'termsofservice', $attributes); ?>
			<br />I Agree <?php echo " " . form_checkbox('termsofservice', 'accept'); ?>
			<p class="help-block">(By checking this box, you agree to our TOS)</p>
		</div>
		
		<?php $attributes = array('class' => 'btn btn-primary', 'name' => 'submit', 'value' => 'Register'); ?>
		<?php echo form_submit($attributes); ?>
			
		<p>Already have an account? <a href="<?php echo site_url("login/"); ?>">Login Now</a>.</p>
	</div>
