<?php
	if ($set_latitude and $set_longitude) {
		$latitude = $set_latitude;
		$longitude = $set_longitude;
	}
	else
	{
		$query = $this->db->where('setting', 'map_starting_position_latitude');
		$query = $this->db->get('settings');
		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $setting)
			{
				$latitude = $setting->value;
			}
		}
		else
		{
			die("Please configure Map Starting Latitude.");
		}
		$query = $this->db->where('setting', 'map_starting_position_longitude');
		$query = $this->db->get('settings');
		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $setting)
			{
				$longitude = $setting->value;
			}
		}
		else
		{
			die("Please configure Map Starting Longitude.");
		}
	}
?>
<div class="container">
	<h1><?php echo $page_title; ?></h1>
	<hr>

	<div class="row">
		<div class="col-md-12">
		<?php
			$error_message = $this->session->flashdata('error_message');
			$success_message = $this->session->flashdata('success_message');
			$info_message = $this->session->flashdata('info_message');
			if ($error_message or validation_errors()) {
				echo '<div class="alert alert-danger"><strong>Sorry... an error occured:</strong><br />'.$error_message . validation_errors() .'</div>';
			}
			if ($success_message) {
				echo '<div class="alert alert-success"><strong>Success!</strong><br />'.$success_message .'</div>';
			}
			if ($info_message) {
				echo '<div class="alert alert-info"><strong>Informational Notice</strong><br />'.$info_message.'</div>';
			}
		?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ul class="nav nav-tabs" id="addEntity">
				<?php if ($set_latitude and $set_longitude) { ?>
					<li><a href="#location" data-toggle="tab">1. Entity Location</a></li>
					<li class="active"><a href="#details" data-toggle="tab">2. Entity Details</a></li>
				<?php } else { ?>
					<li class="active"><a href="#location" data-toggle="tab">1. Entity Location</a></li>
				<?php } ?>
			</ul>

			<div class="tab-content">
				<?php if ($set_latitude and $set_longitude) { ?>
				<div class="tab-pane" id="location">
				<?php } else { ?>
				<div class="tab-pane active" id="location">
				<?php } ?>
					<div class="row">
						<div class="col-md-12" align="center">
							<h3>Entity Location</h3>
							<?php if ($set_latitude and $set_longitude) { ?>
								<small>As specified by pre-entered coordinates...</small><br />
								<small><strong style="color: #FF0000;">Click (or Tap) to change the location on the map below where your entity is.</strong></small>
							<?php } else { ?>
								<small><strong style="color: #FF0000;" id="mapMessage">Click (or Tap) the location on the map below where your entity is.</strong></small>
							<?php } ?>
							<div id="previewMap" style="height:400px; width:80%;"></div>
							<script>
								var map;
								$(document).ready(function(){
								  map = new GMaps({
									div: '#previewMap',
									lat: <?php echo $latitude; ?>,
									lng: <?php echo $longitude; ?>
								  });
								  <?php if ($set_latitude and $set_longitude) { ?>
									map.addMarker({
										lat: <?php echo $latitude;?>,
										lng: <?php echo $longitude;?>,
										title: 'Your New Entity',
										infoWindow: {
										content: '<h3>Your New Entity Will Be Placed Here</h3><div class="alert alert-info"><strong>To Adjust Position</strong>:<ol><li>Find designated area</li><li>Zoom in on designated area</li><li>Right Click designated location</li><li>Click "Adjust Market Position"</li><ol></div>'
										}
									});
								<?php } ?>
								GMaps.on('click', map.map, function(e) {
									window.location.href = "<?php echo site_url('entity/add/')?>/"+e.latLng.lat()+"/"+e.latLng.lng();
									map.addMarker({
										lat: e.latLng.lat(),						
										lng: e.latLng.lng(),
										title: 'Adding New Marker Here...'
									});
								});				
								
								$(document).on('click', '#addEntity', function () {
									$("#previewMap").height( '400px' );
									$("#previewMap").width( '80%' );
									map.refresh();
									map.setCenter(<?php echo $latitude;?>, <?php echo $longitude;?>);
								});
								
								$(document).on('click', '#myLocation', function () {
									GMaps.geolocate({
										success: function(position) {
											map.setCenter(position.coords.latitude, position.coords.longitude);
											map.setZoom(18);
											$('#mapMessage').html("Centered To Your Location (Accuracy will depend on your device's GPS)");
											$('#mapMessage').css('color', 'green');
										  },
										  error: function(error) {
											alert('Geolocation Failed: '+error.message);
										  },
										  not_supported: function() {
											alert("Your browser does not support geolocation");
										  },
										  always: function() {
											alert("Done!");
										  }
										});
								});
								
							});
							</script>
							
							<div class="pull-right" style="padding-top:5px;"><button class="btn btn btn-info" id="myLocation">Zoom On Me</button><br /><small>(Requires Geolocation)</small></div>
							
						</div>
					</div>
					<br />
					<div class="row">
						<div class="col-md-12" align="center">
							<button class="btn btn-lg btn-success" onclick="$('#addEntity a:last').tab('show')">Continue...</button>
						</div>
					</div>
				</div>
				<?php if ($set_latitude and $set_longitude) { ?>
				<div class="tab-pane active" id="details">
				<?php } else { ?>
				<div class="tab-pane" id="details">
				<?php } ?>
					<h3>Entity Details Location</h3>
					<div class="row">
						<?php $attributes = array('class' => 'form-horizontal'); ?>
						<?php echo form_open('entity/add', $attributes); ?>
							<?php echo form_hidden('entity_latitude', $latitude); ?>
							<?php echo form_hidden('entity_longitude', $longitude); ?>
							
							<div class="form-group">
								<?php $attributes = array('class' => 'col-sm-3 control-label'); ?>
								<?php echo form_label('Entity Name', 'entity_name', $attributes); ?>
								<div class="col-sm-9">
									<?php $data = array('class' => 'form-control', 'name' => 'entity_name', 'placeholder' => "Name of Entity (Eg. Location)", 'value' => set_value('entity_name')); ?>
									<?php echo form_input($data); ?>
								</div>
							</div>
							<div class="form-group">
								<?php $attributes = array('class' => 'col-sm-3 control-label'); ?>
								<?php echo form_label('Entity Description (Optional)', 'entity_desc', $attributes); ?>
								<div class="col-sm-9">
									<?php $data = array('class' => 'form-control', 'name' => 'entity_desc', 'placeholder' => "Description of Entity (Eg. Behind Bushes)", 'value' => set_value('entity_desc')); ?>
									<?php echo form_input($data); ?>
								</div>
							</div>
							<div class="form-group">
								<?php $attributes = array('class' => 'col-sm-3 control-label'); ?>
								<?php echo form_label('Category', 'entity_category', $attributes); ?>
								<div class="col-sm-9">
									<select class="form-control" name="entity_category">
										<?php
											$query = $this->db->get('entity_categories');
											
											if ($query->num_rows() > 0)
											{
												foreach ($query->result() as $row)
												{
													$category_id = $row->category_id;
													$category_tag = $row->category_tag;
													$category_name = $row->category_name;
													echo '<option value="'.$category_id.'">['.$category_tag.'] '.$category_name.'</option>';
												}
											}
											else
											{
												echo "Unfortunately you have no categories. You need <em>at least 1 category</em> before you can create an Entity.";
											}
										?>
									</select>
								</div>
							</div>
					</div>
					<br />
					<div class="row">
						<div class="col-md-12" align="center">
							<?php $attributes = array('class' => 'btn-lg btn-primary', 'name' => 'submit', 'value' => 'Add New Entity'); ?>
							<?php echo form_submit($attributes); ?>
						</div>
					</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>