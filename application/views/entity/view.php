<div class="container">
	<div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
				<li>
					<a href="<?php echo site_url('/'); ?>">Overview</a>
				</li>
				<li>
					<a href="#" style="color:#<?php echo $entity_category_color; ?>"><strong><?php echo $entity_category_name; ?></strong></a>
				</li>
				<li class="active">
					<strong><?php echo $entity_name; ?></strong>
				</li>
          </ul>
		  <?php
				$success_message = $this->session->flashdata('success_message');
				$error_message = $this->session->flashdata('error_message');
				
				if ($success_message)
				{
					echo '<div class="alert alert-success">'.$success_message.'</div>';
					echo '<hr>';
				}
				if ($error_message or validation_errors())
				{
					echo '<div class="alert alert-danger">'.$error_message.'</div>';
					echo '<hr>';
				}
			?>
		</div>
	</div>
          <div class="row">
            <div class="col-md-6" style="">
              <div class="panel panel-default" style="">
                <div class="panel-heading">
                  <h3 class="panel-title">
                    <span class="glyphicon glyphicon-info-sign"></span> General Information
                  </h3>
                </div>
                <div class="panel-body" style="padding:0px; max-height:100%; overflow-y:visible;">
					<table class="table table-striped">
					  <tbody>
						<tr>
						  <th>Name</th>
						  <td><?php echo $entity_name; ?></td>
						  <td><a href="#" class="btn-sm btn-warning pull-right modal_edituser" data-toggle="modal" data-id="'.$row->user_id.'"  data-fullname="'.$row->full_name.'" data-useremail="'.$row->user_email.'" data-userlevel="'.$row->user_level.'"  data-target="#edit_user"><span class="glyphicon glyphicon-pencil"></span> </a></td>
						</tr>
						<?php if ($entity_desc) { ?>
						<tr>
						  <th>Description</th>
						  <td><?php echo $entity_desc; ?></td>
						  <td><a href="#" class="btn-sm btn-warning pull-right modal_edituser" data-toggle="modal" data-id="'.$row->user_id.'"  data-fullname="'.$row->full_name.'" data-useremail="'.$row->user_email.'" data-userlevel="'.$row->user_level.'"  data-target="#edit_user"><span class="glyphicon glyphicon-pencil"></span> </a></td>
						</tr>
						<?php } ?>
						<tr>
						  <th>Category</th>
						  <td><?php echo $entity_category_name; ?></td>
						  <td><a href="#" class="btn-sm btn-warning pull-right modal_edituser" data-toggle="modal" data-id="'.$row->user_id.'"  data-fullname="'.$row->full_name.'" data-useremail="'.$row->user_email.'" data-userlevel="'.$row->user_level.'"  data-target="#edit_user"><span class="glyphicon glyphicon-pencil"></span> </a></td>
						</tr>
						<?php if ($entity_reference_location) { ?>
						<tr>
						  <th>Reference Location</th>
						  <td><?php echo $entity_reference_location; ?></td>
						  <td><a href="#" class="btn-sm btn-warning pull-right modal_edituser" data-toggle="modal" data-id="'.$row->user_id.'"  data-fullname="'.$row->full_name.'" data-useremail="'.$row->user_email.'" data-userlevel="'.$row->user_level.'"  data-target="#edit_user"><span class="glyphicon glyphicon-pencil"></span> </a></td>
						</tr>
						<?php } ?>
						<tr>
						  <th>Coordinates</th>
						  <td>Latitude: <?php echo $entity_latitude; ?><br />Longitude: <?php echo $entity_longitude; ?></td>
						  <td><a href="#" class="btn-sm btn-warning pull-right modal_edituser" data-toggle="modal" data-id="'.$row->user_id.'"  data-fullname="'.$row->full_name.'" data-useremail="'.$row->user_email.'" data-userlevel="'.$row->user_level.'"  data-target="#edit_user"><span class="glyphicon glyphicon-pencil"></span> </a></td>
						</tr>
						<tr>
						  <th>Status</th>
						  <td><span class="label label-default" style="background-color: #<?php echo $entity_status_color; ?>"><?php echo $entity_status_name; ?></span> <abbr class="tip" original-title="<?php// echo standard_date('DATE_RFC1123', strtotime($entity_last_status_time)); ?>"><?php// echo timespan(strtotime($entity_last_status_time), time()) . ' ago'; ?></abbr></td>
						  <td><a href="#" class="btn-sm btn-warning pull-right modal_edituser" data-toggle="modal" data-id="'.$row->user_id.'"  data-fullname="'.$row->full_name.'" data-useremail="'.$row->user_email.'" data-userlevel="'.$row->user_level.'"  data-target="#edit_user"><span class="glyphicon glyphicon-pencil"></span> </a></td>
						</tr>
						<tr>
						  <th>Next Scheduled Check In</th>
						  <td><span class="badge">19 days</span> on the 20/04/2014</td>
						  <td><a href="#" class="btn-sm btn-warning pull-right modal_edituser" data-toggle="modal" data-id="'.$row->user_id.'"  data-fullname="'.$row->full_name.'" data-useremail="'.$row->user_email.'" data-userlevel="'.$row->user_level.'"  data-target="#edit_user"><span class="glyphicon glyphicon-pencil"></span> </a></td>
						</tr>
						<tr>
						  <th>Last Viewed by</th>
						  <td><?php echo anchor('profile/view/'.$entity_last_viewed_by, $entity_last_viewed_by_name); ?> <small><abbr class="tip" original-title="<?php echo standard_date('DATE_RFC1123', strtotime($entity_last_viewed_time)); ?>"><?php echo timespan(strtotime($entity_last_viewed_time), time()) . ' ago'; ?></abbr></small></td>
						  <td><a href="#" class="btn-sm btn-warning pull-right modal_edituser" data-toggle="modal" data-id="'.$row->user_id.'"  data-fullname="'.$row->full_name.'" data-useremail="'.$row->user_email.'" data-userlevel="'.$row->user_level.'"  data-target="#edit_user"><span class="glyphicon glyphicon-pencil"></span> </a></td>
						</tr>
						<tr>
						  <th>Last Updated by</th>
						  <td> <?php echo anchor('profile/view/'.$entity_last_updated_by, $entity_last_updated_by_name); ?> <small><abbr class="tip" original-title="<?php echo standard_date('DATE_RFC1123', strtotime($entity_last_updated_time));?>"><?php echo timespan(strtotime($entity_last_updated_time), time()) . ' ago'; ?></abbr></small></td>
						  <td><a href="#" class="btn-sm btn-warning pull-right modal_edituser" data-toggle="modal" data-id="'.$row->user_id.'"  data-fullname="'.$row->full_name.'" data-useremail="'.$row->user_email.'" data-userlevel="'.$row->user_level.'"  data-target="#edit_user"><span class="glyphicon glyphicon-pencil"></span> </a></td>
						</tr>
					  </tbody>
					</table>
					<div align="center"><strong><em>End of General Information</em></strong></div>
                  <div class="pull-right">
                    <div class="btn-group">
                      <ul class="dropdown-menu" role="menu">
                        <li>
                          <a href="#">Action</a>
                        </li>
                        <li>
                          <a href="#">Another action</a>
                        </li>
                        <li>
                          <a href="#">Something else here</a>
                        </li>
                        <li class="divider">
                        </li>
                        <li>
                          <a href="#">Separated link</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6" style="display: block;">
			<div class="row">
			<div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title">
                    <span class="glyphicon glyphicon-screenshot"></span> Map View <button href="#" class="btn-sm btn-info pull-right" style="padding:0;" data-toggle="modal" data-target="#mapModal">View Larger Map</a>
                  </h3>
                </div>
                <div class="panel-body" style="overflow-y:visible; padding:0px;" align="center">
                  <div id="googleMapSingle" style="height:240px; width:100%;"></div>
                </div>
              </div>
            </div>
		<div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">
                <span class="glyphicon glyphicon-comment"></span> Comments <button href="#" class="btn-sm btn-info pull-right" style="padding:0;" data-toggle="modal" data-target="#commentModal">Leave Comment</a>
              </h3>
            </div>
            <div class="panel-body">
				<?php
					$this->db->join('users', 'users.user_id = entity_comments.comment_by');
					$this->db->where('entity_id', $entity_id);
					$this->db->order_by('comment_id', 'desc');
					$comments = $this->db->get('entity_comments');
					if ($comments->num_rows() > 0)
					{
						$commentnum = $comments->num_rows() + 1;
						foreach ($comments->result() as $comment)
						{
							$commentnum = $commentnum - 1;
							$authorid = $comment->comment_by;
							$author = $comment->full_name;
							$comment_message = $comment->comment;
							$comment_time = $comment->comment_time;
				?>
					<div>
						<strong><?php echo anchor('profile/view/'.$authorid, $author); ?></strong> <small>#<?php echo $commentnum; ?></small> <small class="pull-right"><abbr class="tip" original-title="<?php echo standard_date('DATE_RFC1123', strtotime($comment_time)); ?>"><?php echo timespan(strtotime($comment_time), time()) . ' ago'; ?></abbr></small>
						<div class="alert alert-info"><?php echo $comment_message; ?></div>
					</div>
				
				<?php
						}
					}
					else {
						echo '<div class="alert alert-info">No comments have been left.</div>';
					}
				?>
				<div align="center"><strong><em>End of Comments (Total <?php echo $comments->num_rows(); ?>)</em></strong></div>
			</div>
          </div>
        </div>
	</div>
      
	 
	<div class="modal fade" id="commentModal" tabindex="-1" role="dialog">
		<div class="modal-dialog">
		<?php $attributes = array('class' => 'form', 'role' => 'form')?>
		<?php echo form_open('entity/view/'.$entity_id.'/comment', $attributes); ?>
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Leave Comment on <?php echo $entity_name; ?></h4>
				</div>
				<div class="modal-body" align="center">
					<div class="form-group">
						<label for="full_name" class="ontrol-label">Your Comment</label>
							<?php $attributes = array('class' => 'form-control', 'name' => 'comment', 'rows' => '5'); ?>
							<?php echo form_textarea($attributes); ?>
							<p class="help-block">This is the name the user will be referred to throughout the system.</p>
					</div>
				</div>
				<div class="modal-footer">
					<?php $attributes = array('class' => 'btn btn-primary', 'name' => 'submit', 'value' => 'Add Comment'); ?>
					<?php echo form_submit($attributes); ?>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		<?php echo form_close(); ?>
		</div>
	</div>	 
	
	<div class="modal fade" id="mapModal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" style="height:95%; width:95%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><?php echo $entity_name; ?></h4>
					<h5><?php echo $entity_desc; ?></h5>
				</div>
				<div class="modal-body" style="padding:0px; overflow-y:visible;" align="center">
					<div style="height:600px !important; width:100% !important;">
						<div id="googleMapSingleModal" style="height:600px !important;"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>