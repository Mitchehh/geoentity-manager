    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand">Smiley Face</h3>
              <ul class="nav masthead-nav">
                <li class="active"><a href="<?php echo site_url(); ?>">Home</a></li>
                <?php if ($this->session->userdata('logged_in')) { ?>
                <li><a href="<?php echo site_url('dashboard'); ?>">Dashboard</a></li>
                <li><a href="<?php echo site_url('logout'); ?>">Logout</a></li>
                <?php } else { ?>
                <li><a href="<?php echo site_url('register'); ?>">Register</a></li>
                <li><a href="<?php echo site_url('login'); ?>">Login</a></li>
                <?php } ?>
              </ul>
            </div>
          </div>