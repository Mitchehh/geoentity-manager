<!DOCTYPE html>
<html lang="en">
  
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>
		<?php echo $page_title; ?>
    </title>
    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css"
    rel="stylesheet">
  </head>
  
  <body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"
    >
    </script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"
    >
    </script>
    <div class="container" style="margin-top:3%">
      <div class="row">
        <div class="col-md-12" align="center">
          <img style="height:150px;" src="https://s3.amazonaws.com/media.jetstrap.com/BRhgS4b6SMywvhl1OGpf_RFS Logo.jpg">
          <h3>
            Forest Hill Rural Fire Brigade
            <br>
            Hydrant Manager
          </h3>
          <div class="form-group">
			<?php
				$success_message = $this->session->flashdata('success_message');
				$error_message = $this->session->flashdata('error_message');
				
				if ($success_message)
				{
					echo '<div class="alert alert-success">'.$success_message.'</div>';
					echo '<hr >';
				}
				if ($error_message or validation_errors())
				{
					echo '<div class="alert alert-danger"><strong>Sorry... an error occured:</strong><br />'.$error_message . validation_errors() .'</div>';
					echo '<hr >';
				}
			?>
			<?php echo form_open('login'); ?>
		
			<div class="form-group">
				<?php $data = array('class' => 'form-control', 'name' => 'email', 'placeholder' => 'Email Address'); ?>
				<?php echo form_input($data); ?>
			</div>
	
			<div class="form-group">
				<?php $data = array('class' => 'form-control', 'name' => 'password', 'placeholder' => 'Password'); ?>
				<?php echo form_password($data); ?>
			</div>
	
			<?php $attributes = array('class' => 'btn-lg btn-primary', 'name' => 'submit', 'value' => 'Login'); ?>
			<?php echo form_submit($attributes); ?>
            or <a class="btn btn-link" href="#">Forgot Your Password?</a>
				
			<?php echo form_close(); ?>

          </div>
			<div align="center">
				<p>
				Access to this system is for Forest Hill Rural Fire Brigade authorised
				individuals only.
				</p>
				<div>
				If you believe you need access, please contact your Brigade Captain or
				Hydrant Manager Administrator.
				</div>
			</div>
        </div>
      </div>
    </div>
  </body>

</html>
