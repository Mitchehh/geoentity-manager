<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	function audit_log($message, $success, $initiated_by = -1, $ipaddress = NULL)
	{
		$ci = get_instance();
		$ci->load->helper('text'); 
		
		if (!$ipaddress) {
			$ipaddress = $ci->input->ip_address();
		}
		$data = array(
			'log_message' => ascii_to_entities($message),
			'log_success' => $success,
			'log_initiated_by' => $initiated_by,
			'log_ipaddress' => ascii_to_entities($ipaddress)
		);

		$ci->db->insert('logs', $data);
	}