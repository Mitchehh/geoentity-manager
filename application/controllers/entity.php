<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Entity extends CI_Controller {

	public function index()
	{
		if ($this->session->userdata('logged_in') != TRUE) {
			redirect("login");
		}
		redirect('viewall');
	}
	public function view($entity_id = FALSE, $method = FALSE)
	{
		if ($this->session->userdata('logged_in') != TRUE) {
			redirect('login');
		}
		if (!is_numeric($entity_id)) {
			redirect('');		
		}
		$user_id = $this->session->userdata('logged_in');
		
		$this->db->where('entity_id', $entity_id);
		$query = $this->db->get('entities');
		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $entity)
			{
				$entity_data['entity_id'] = $entity->entity_id;
				$entity_data['entity_category'] = $entity->entity_category;
				$entity_data['entity_name'] = $entity->entity_name;
				$entity_data['entity_desc'] = $entity->entity_desc;
				$entity_data['entity_status'] = $entity->entity_status;
				$entity_data['entity_latitude'] = $entity->entity_latitude;
				$entity_data['entity_longitude'] = $entity->entity_longitude;
				$entity_data['entity_reference_location'] = $entity->entity_reference_location;
				$entity_data['entity_last_viewed_by'] = $entity->entity_last_viewed_by ?: FALSE;
				$entity_data['entity_last_viewed_time'] = $entity->entity_last_viewed_time ?: FALSE;
				$entity_data['entity_last_updated_by'] = $entity->entity_last_updated_by;
				$entity_data['entity_last_updated_time'] = $entity->entity_last_updated_time;
			}
			
			// METHODS
			if ($method == "comment") {
				$input_comment = quotes_to_entities($this->input->post('comment'));
					$data = array(
						'entity_id' => $entity->entity_id,
						'comment_by' => $this->session->userdata('user_id'),
						'comment' => $input_comment
					);
				
				$this->db->insert('entity_comments', $data);
				audit_log("New Comment On: ".$entity->entity_name." by ".$this->session->userdata('full_name')."", TRUE, $this->session->userdata('user_id'), $this->input->ip_address());
				$this->session->set_flashdata('success_message', "Successfully Added Comment: ".$input_comment);
			}

		
		// END METHODS
			
			$this->db->where('status_id', $entity->entity_status);
			$query1 = $this->db->get('entity_statuses');
			if ($query1->num_rows() == 1)
			{
				foreach ($query1->result() as $entity_status)
				{
					$entity_data['entity_status_category_id'] = $entity_status->entity_category_id;
					$entity_data['entity_status_name'] = $entity_status->status_name;
					$entity_data['entity_status_color'] = $entity_status->status_color;
				}
			}
			else {
				$entity_data['entity_status_category_id'] = -1;
				$entity_data['entity_status_name'] = "<small>STATUS NOT SET</small>";
				$entity_data['entity_status_color'] = "000000; color:#FFD700";
			}
			
			$this->db->where('category_id', $entity->entity_category);
			$query2 = $this->db->get('entity_categories');
			if ($query2->num_rows() == 1)
			{
				foreach ($query2->result() as $entity_category)
				{
					$entity_data['entity_category_name'] = $entity_category->category_name;
					$entity_data['entity_category_tag'] = $entity_category->category_tag;
					$entity_data['entity_category_color'] = $entity_category->category_color;
				}
			}
			
			if ($entity_data['entity_last_viewed_by']) {
				$this->db->where('user_id', $entity->entity_last_viewed_by);
				$query3 = $this->db->get('users');
				if ($query3->num_rows() == 1)
				{
					foreach ($query3->result() as $user)
					{
						$entity_data['entity_last_viewed_by_name'] = $user->full_name;
					}
				}
			}
			else
			{
				$entity_data['entity_last_viewed_time'] = "Never";
				$entity_data['entity_last_viewed_by_name'] = "No One";
			}
			
			$this->db->where('user_id', $entity->entity_last_updated_by);
			$query4 = $this->db->get('users');
			if ($query4->num_rows() == 1)
			{
				foreach ($query4->result() as $user2)
				{
					$entity_data['entity_last_updated_by_name'] = $user2->full_name;
				}
			}
			
			$data['page_title'] = 'View Entity';
			$data['site_name'] = 'Hydrant Manager';
			$this->load->view('_includes/header', $data);
			$this->load->view('_includes/navbar');
			$this->load->view('entity/view', $entity_data);
			$this->load->view('_includes/map_api_resources');
			$this->load->view('_includes/map_view_single',$entity_data);
			$this->load->view('_includes/footer');
			
			
			$data = array(
				'entity_last_viewed_by' => $this->session->userdata('user_id'),
				'entity_last_viewed_time' => date('Y-m-d H:i:s')
			);
			
			$this->db->where('entity_id', $entity_data['entity_id']);
			$this->db->update('entities', $data);
		}
		else
		{
			redirect('overview');
		}
	}

	public function add($lat = '', $long = '')
	{
		if ($this->session->userdata('logged_in') != TRUE) {
			redirect("login");
		}
		if ($this->input->post()) {
			$this->load->library('form_validation');
			$this->load->helper('string');
			$this->form_validation->set_rules('entity_name', 'Entity Name', 'required|max_length[255]|strip_tags|is_unique[entities.entity_name]');
			$this->form_validation->set_rules('entity_desc', 'Entity Description', 'max_length[255]|strip_tags');
			$this->form_validation->set_rules('entity_category', 'Category', 'required');
			$this->form_validation->set_rules('entity_latitude', 'Entity Latitude', 'required|numeric');
			$this->form_validation->set_rules('entity_longitude', 'Entity Latitude', 'required|numeric');
			
			if ($this->form_validation->run() == TRUE)
			{
				$input_name = quotes_to_entities($this->input->post('entity_name'));
				$input_desc = quotes_to_entities($this->input->post('entity_desc'));
				$input_category = $this->input->post('entity_category');
				$input_latitude = $this->input->post('entity_latitude');
				$input_longitude = $this->input->post('entity_longitude');

				$user_id = $this->session->userdata('user_id');

				$data = array(
					'entity_name' => $input_name,
					'entity_desc' => $input_desc,
               		'entity_category' => $input_category,
               		'entity_latitude' => $input_latitude,
               		'entity_longitude' => $input_longitude,
               		'entity_created_by' => $user_id,
               		'entity_last_updated_by' => $user_id,
               		'entity_last_updated_time' => date('Y-m-d H:i:s')
            	);

				$this->db->insert('entities', $data);
				
				audit_log("Created New Entity: ".$input_name ." (Desc: ".$input_desc.")", TRUE, $user_id, $this->input->ip_address());	
				$this->session->set_flashdata('success_message', "Successfully created Entity: ".$input_name);
				
				redirect('/');
			}
		}
		$data['page_title'] = 'Add New Entity';
		
		if($lat and $long) {
			$data['set_latitude'] = $lat;
			$data['set_longitude'] = $long;
			$this->session->set_flashdata('info_message', 'Latitude has been set as '.$lat.'<br />Longitude has been set as '.$long);
			$this->session->set_flashdata('error_message', NULL);
		}
		else {
			$data['set_latitude'] = $lat;
			$data['set_longitude'] = $long;
			//$this->session->set_flashdata('error_message', 'Please set your entity\'s location on the right map before continuing...<br /><strong>To Set A Position</strong>:<ol><li>Find designated area</li><li>Zoom in on designated area</li><li>Right Click designated location</li><li>Click "Adjust Market Position"</li><ol>');
		}
			
		$this->load->view('_includes/header', $data);
		$this->load->view('_includes/navbar');
		$this->load->view('_includes/map_api_resources');
		$this->load->view('entity/add', $data);
		$this->load->view('_includes/footer');
	}
}

/* End of file questions.php */
/* Location: ./application/controllers/questions.php */