<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function index()
	{
		if ($this->session->userdata('logged_in') != TRUE) {
			redirect('login');
		}
		redirect('profile/view');
		
	}
	public function view($id)
	{
		if ($id) {
			$this->db->where('user_id', $id);
			$query = $this->db->get('users');

			if ($query->num_rows() == 1) {
				$data['page_title'] = 'View Profile';
				$data['user_id'] = $id;
				$this->load->view('_includes/header', $data);
				$this->load->view('_includes/navbar');
				$this->load->view('profile/view', $data);
				$this->load->view('_includes/footer');
			}
			else {
				$this->session->set_flashdata('error_message', 'Sorry... That user profile could not be found.');
				redirect('profile/view/'. $this->session->userdata('user_id'));
				exit();
			}
		}
		else {
			redirect('profile/view/'. $this->session->userdata('user_id'));
			exit();
		}
	
	}
}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */