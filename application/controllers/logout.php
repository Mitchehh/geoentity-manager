<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller {

	public function index()
	{
		$array_items = array('user_id' => '', 'user_name' => '', 'logged_in' => '');

		$this->session->unset_userdata($array_items);
		
		$this->session->set_flashdata('success_message', 'Successfully logged out.');
		redirect('login');
	}
}

/* End of file logout.php */
/* Location: ./application/controllers/logout.php */