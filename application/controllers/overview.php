<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Overview extends CI_Controller {

	public function index()
	{
		if ($this->session->userdata('logged_in') != TRUE) {
			redirect("login");
		}
		redirect('overview/map');
	}
	public function map()
	{
		if ($this->session->userdata('logged_in') != TRUE) {
			redirect("login");
		}

		$data['site_name'] = 'Hydrant Manager';
		$data['page_title'] = 'Map View';
		$this->load->view('_includes/header', $data);
		$this->load->view('_includes/navbar');
		
		//$this->db->where('user_id', $this->session->userdata('user_id'));
		
		$this->load->view('overview/map');
		$this->load->view('_includes/map_api_resources');
		$this->load->view('_includes/map_view');
		$this->load->view('_includes/footer');
	}
		
	public function entities()
	{
		if ($this->session->userdata('logged_in') != TRUE) {
			redirect("login");
		}

		$data['site_name'] = 'Hydrant Manager';
		$data['page_title'] = 'List View';
		$this->load->view('_includes/header', $data);
		$this->load->view('_includes/navbar');
		
		//$this->db->where('user_id', $this->session->userdata('user_id'));
		
		$this->load->view('overview/entities');
		$this->load->view('_includes/footer');
	}
}

/* End of file questions.php */
/* Location: ./application/controllers/questions.php */