<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {

	public function index()
	{
		if ($this->session->userdata('logged_in') != TRUE) {
			redirect("login");
		}
		redirect('settings/site');
	}
	public function site()
	{
		if ($this->session->userdata('logged_in') != TRUE) {
			redirect("login");
		}
		
		$user_id = $this->session->userdata('user_id');
		
		if ($this->session->userdata('level') > 1) {
			audit_log("Viewed Administration: Site Settings", TRUE, $user_id, $this->input->ip_address());	
			$data['page_title'] = 'Site Settings';
			$this->load->view('_includes/header', $data);
			$this->load->view('_includes/navbar');
			$this->load->view('settings/site');
			$this->load->view('_includes/footer');
		}
		else {
			audit_log("Unauthorized Access to Administration: Site Settings", FALSE, $user_id, $this->input->ip_address());
			$this->session->set_flashdata('error_message', "You do not have access to view users. This attempt has been logged.");
			redirect('/');
		}
	}
	public function users($method = NULL)
	{
		if ($this->session->userdata('logged_in') != TRUE) {
			redirect("login");
		}
		
		$user_id = $this->session->userdata('user_id');
		$user_level = $this->session->userdata('level');
		$full_name = $this->session->userdata('full_name');
		$input_id = $this->input->post('user_id');
		
		if ($this->session->userdata('level') > 1) {
			if ($this->input->post()) {
				if (($method == 'update') && ($this->input->post('action') == 'Delete User...')) {
					if ($input_id == $user_id) {
						audit_log("Attempted to Delete Own User", FALSE, $user_id, $this->input->ip_address());
						$this->session->set_flashdata('error_message', "You can't delete yourself... Why would you do that ".$full_name."? :(");
						redirect('settings/users');
						exit();
					}
					$input_id = (int)quotes_to_entities($this->input->post('user_id'));
					if (!is_int($input_id)) {
						$this->session->set_flashdata('error_message', "A Proper User was not specified. Please try again. If this problem persists, contact your Administrator.");
						redirect('settings/users');
					}
					$this->db->delete('users', array('user_id' => $input_id)); 
					audit_log("Deleted User: ".$this->input->post('full_name')." (".$this->input->post('email').")", TRUE, $user_id, $this->input->ip_address());
					$this->session->set_flashdata('success_message', "Successfully deleted User: ".quotes_to_entities($this->input->post('full_name')));
					redirect('settings/users');
					exit();
					}
				}
				$this->load->library(array('encrypt', 'form_validation'));
				$this->form_validation->set_rules('full_name', 'Full Name', 'required|max_length[255]|strip_tags');
					
				if ($method == 'add') {
					$this->form_validation->set_rules('user_email', 'User Email', 'required|max_length[255]|strip_tags|valid_email|is_unique[users.user_email]');
					$this->form_validation->set_rules('user_password', 'User Password', 'required');
				}
				else {
					$this->form_validation->set_rules('user_email', 'User Email', 'required|max_length[255]|strip_tags|valid_email');
				}
				$this->form_validation->set_rules('user_level', 'User Level', 'required|max_length[1]|integer');
				
				if ($this->form_validation->run() == TRUE)
				{
					$input_name = quotes_to_entities($this->input->post('full_name'));
					$input_email = quotes_to_entities($this->input->post('user_email'));
					$input_password = $this->input->post('user_password');
					if ($input_password) {
						$hash = $this->encrypt->sha1($input_password);
					}
					$input_level = $this->input->post('user_level');
					if ($method == "add") {
						$data = array(
							'full_name' => $input_name,
							'user_email' => $input_email,
							'user_password' => $hash,
							'user_level' => $input_level
						);
						if ($input_level == 1) {
							$level = "Officer";
						}
						elseif ($input_level == 2) {
							$level = "Administrator";
						}
						else {
							$level = "Unknown";
						}
						$this->db->insert('users', $data);
						audit_log("Created New User: ".$input_name." (".$input_email.") as ".$level." (Level: ".$input_level.")", TRUE, $user_id, $this->input->ip_address());
						$this->session->set_flashdata('success_message', "Successfully created New User: ".$input_name);
					}
					elseif ($method == "update") {
						if ($this->input->post('user_password')) { // Update password?
							$data = array(
								'full_name' => $input_name,
								'user_email' => $input_email,
								'user_password' => $hash,
								'user_level' => $input_level
							);
						}
						else { // Nope.
							$data = array(
								'full_name' => $input_name,
								'user_email' => $input_email,
								'user_level' => $input_level
							);
						}
						$input_id = (int)$this->input->post('user_id');
						if (!is_int($input_id)) {
							$this->session->set_flashdata('error_message', "A Proper User ID was not specified. Please try again. If this problem persists, contact your Administrator.");
							redirect('settings/users');
						}
						$this->db->where('user_id', $this->input->post('user_id'));
						$this->db->update('users', $data); 
						audit_log("Updated User: ".$input_name." (".$input_email.") Rank: ".$level." (Level: ".$input_level.")", TRUE, $user_id, $this->input->ip_address());
						$this->session->set_flashdata('success_message', "Successfully updated User: ".$input_name);
					}
					redirect('settings/users');
				}
			
			audit_log("Viewed Administration: User Management", TRUE, $user_id, $this->input->ip_address());	
			$data['page_title'] = 'User Management';
			$this->load->view('_includes/header', $data);
			$this->load->view('_includes/navbar');
			$this->load->view('settings/users');
			$this->load->view('_includes/modals/add_user');
			$this->load->view('_includes/modals/edit_user');
			$this->load->view('_includes/footer');
		}
		else {
			audit_log("Unauthorized Access to Administration: Users", FALSE, $user_id, $this->input->ip_address());
			$this->session->set_flashdata('error_message', "You do not have access to view users. This attempt has been logged.");
			redirect('/');
		}
		
	}

	public function categories($what = NULL, $method = NULL)
	{
		if ($this->session->userdata('logged_in') != TRUE) {
			redirect("login");
		}
		
		$user_id = $this->session->userdata('user_id');
		$user_level = $this->session->userdata('level');
		
		if ($this->session->userdata('level') > 1) {
			if (($method == 'update') && ($this->input->post('action') == 'Delete Status...')) {
				$input_id = (int)quotes_to_entities($this->input->post('status_id'));
				if (!is_int($input_id)) {
					$this->session->set_flashdata('error_message', "A Proper Entity Status was not specified. Please try again. If this problem persists, contact your Administrator.");
					redirect('settings/categories');
				}
				$this->db->delete('entity_statuses', array('status_id' => $input_id)); 
				audit_log("Deleted Status: ".$this->input->post('status_name'), TRUE, $user_id, $this->input->ip_address());
				$this->session->set_flashdata('success_message', "Successfully deleted Status: ".quotes_to_entities($this->input->post('status_name')));
				redirect('settings/categories');
				exit();
			}
			if (($method == 'update') && ($this->input->post('action') == 'Delete Category...')) {
				$input_id = (int)quotes_to_entities($this->input->post('category_id'));
				if (!is_int($input_id)) {
					$this->session->set_flashdata('error_message', "A Proper Entity Status was not specified. Please try again. If this problem persists, contact your Administrator.");
					redirect('settings/categories');
				}
				$this->db->delete('entity_categories', array('category_id' => $input_id)); 
				audit_log("Deleted Category: ".$this->input->post('category_name'), TRUE, $user_id, $this->input->ip_address());
				$this->session->set_flashdata('success_message', "Successfully deleted Category: ".quotes_to_entities($this->input->post('category_name')));
				redirect('settings/categories');
				exit();
			}
			if ($this->input->post()) {
				
				$this->load->library('form_validation');
				if ($what == "category") {
					if ($method == "update") {
						$this->form_validation->set_rules('category_name', 'Category Name', 'required|max_length[255]|strip_tags');
						$this->form_validation->set_rules('category_tag', 'Category Tag', 'required|max_length[255]|strip_tags');
					}
					else {
						$this->form_validation->set_rules('category_name', 'Category Name', 'required|max_length[255]|strip_tags|is_unique[entity_categories.category_name]');
						$this->form_validation->set_rules('category_tag', 'Category Tag', 'required|max_length[255]|strip_tags');
					}
					$this->form_validation->set_rules('category_color', 'Category Color', 'required|max_length[6]|strip_tags');
				} elseif ($what == "status") {
					$this->form_validation->set_rules('status_name', 'Status Name', 'required|max_length[255]|strip_tags');
					$this->form_validation->set_rules('status_category', 'Assign to Category', 'required|integer');
					$this->form_validation->set_rules('status_color', 'Status Color', 'required|max_length[6]|strip_tags');
				}
				
				if ($this->form_validation->run() == TRUE)
				{
					if ($what == "category") {
						$input_name = quotes_to_entities($this->input->post('category_name'));
						$input_tag = quotes_to_entities(strtoupper($this->input->post('category_tag')));
						$input_color = $this->input->post('category_color');
						
						$data = array(
							'category_name' => $input_name,
							'category_tag' => $input_tag,
							'category_color' => $input_color
						);
						
						if ($method == "add") {
							$this->db->insert('entity_categories', $data);
							audit_log("Created Category: ".$input_name, TRUE, $user_id, $this->input->ip_address());
							$this->session->set_flashdata('success_message', "Successfully created Category: ".$input_name);
						}
						elseif ($method == "update") {
							$input_id = (int)$this->input->post('category_id');
							if (!is_int($input_id)) {
								$this->session->set_flashdata('error_message', "A Proper Category was not specified. Please try again. If this problem persists, contact your Administrator.");
								redirect('settings/categories');
							}
							$this->db->where('category_id', $this->input->post('category_id'));
							$this->db->update('entity_categories', $data); 
							audit_log("Updated Category: ".$input_name, TRUE, $user_id, $this->input->ip_address());
							$this->session->set_flashdata('success_message', "Successfully updated Category: ".$input_name);
						}
					}elseif ($what == "status") {
						$input_name = quotes_to_entities(strtoupper($this->input->post('status_name')));
						$input_category = quotes_to_entities($this->input->post('status_category'));
						$input_color = $this->input->post('status_color');
						
						$data = array(
							'status_name' => $input_name,
							'entity_category_id' => $input_category,
							'status_color' => $input_color
						);
						
						if ($method == "add") {
							$this->db->insert('entity_statuses', $data);
							audit_log("Created Status: ".$input_name, TRUE, $user_id, $this->input->ip_address());
							$this->session->set_flashdata('success_message', "Successfully created Status: ".$input_name);
						}
						elseif ($method == "update") {
							$input_id = (int)$this->input->post('status_id');
							if (!is_int($input_id)) {
								$this->session->set_flashdata('error_message', "A Proper Status was not specified. Please try again. If this problem persists, contact your Administrator.");
								redirect('settings/categories');
							}
							$this->db->where('status_id', $this->input->post('status_id'));
							$this->db->update('entity_statuses', $data); 
							audit_log("Updated Status: ".$input_name, TRUE, $user_id, $this->input->ip_address());
							$this->session->set_flashdata('success_message', "Successfully updated Status: ".$input_name);
						}
					}
					
					redirect('settings/categories');
				}
			}
			audit_log("Viewed Administration: Categories & Statuses Management", TRUE, $user_id, $this->input->ip_address());	
			$data['page_title'] = 'Categories & Statuses Management';
			$this->load->view('_includes/header', $data);
			$this->load->view('_includes/navbar');
			$this->load->view('settings/categories');
			$this->load->view('_includes/modals/add_category');
			$this->load->view('_includes/modals/edit_category');
			$this->load->view('_includes/modals/add_entity_status');
			$this->load->view('_includes/modals/edit_entity_status');
			$this->load->view('_includes/pick_a_color_resources');
			$this->load->view('_includes/footer');
		}
		else  {
			$this->session->set_flashdata('error_message', "You do not have access to view categories. This attempt has been logged.");
			audit_log("Unauthorized Access to Administration: Categories", FALSE, $user_id, $this->input->ip_address());
			redirect('/');
		}
	}
	public function entities()
	{
		if ($this->session->userdata('logged_in') != TRUE) {
			redirect("login");
		}
		$user_id = $this->session->userdata('user_id');
		
		if ($this->session->userdata('level') > 1) {
			audit_log("Viewed Administration: Entity Management", TRUE, $user_id, $this->input->ip_address());	
			$data['page_title'] = 'Entity Management';
			$this->load->view('_includes/header', $data);
			$this->load->view('_includes/navbar');
			$this->load->view('settings/entities');
			$this->load->view('_includes/footer');
		}
		else {
			audit_log("Unauthorized Access to Administration: Entity Management", FALSE, $user_id, $this->input->ip_address());
			$this->session->set_flashdata('error_message', "You do not have access to view logs. This attempt has been logged.");
			redirect("/");
		}
	}
	public function logs()
	{
		if ($this->session->userdata('logged_in') != TRUE) {
			redirect("login");
		}
		
		$user_id = $this->session->userdata('user_id');
				
		if ($this->session->userdata('level') > 1) {	
			audit_log("Viewed Administration: Logs", TRUE, $user_id, $this->input->ip_address());	
			$data['page_title'] = 'Logs';
			$this->load->view('_includes/header', $data);
			$this->load->view('_includes/navbar');
			$this->load->view('settings/logs');
			$this->load->view('_includes/footer');
		}
		else {
			audit_log("Unauthorized Access to Administration: Logs", FALSE, $user_id, $this->input->ip_address());
			$this->session->set_flashdata('error_message', "You do not have access to view logs. This attempt has been logged.");
			redirect('/');
		}
	}
}

/* End of file questions.php */
/* Location: ./application/controllers/questions.php */