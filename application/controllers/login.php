<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		if ($this->session->userdata('logged_in')) {
			redirect("overview");
		}
		
		if ($this->input->post()) {
			$this->load->library(array('encrypt', 'form_validation'));

			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			$this->form_validation->set_rules('password', 'Password', 'required');
			
			$this->form_validation->set_message('required', '%s is required!');
			$this->form_validation->set_message('valid_email', 'Please enter a valid email!');

			if ($this->form_validation->run() == TRUE) {
				$email = $this->input->post('email');
				$password = $this->input->post('password');
				$hash = $this->encrypt->sha1($password);

				$data = array(
               		'user_email' => $email,
               		'user_password' => $hash
            	);

            	$this->db->where($data);
				$query = $this->db->get('users');

				if ($query->num_rows() == 1) {
					foreach ($query->result() as $row)
					{
    					$user_id = $row->user_id;
    					$full_name = $row->full_name;
    					$user_logins = $row->user_logins;
    					$user_level = $row->user_level;
    				}
					$newdata = array(
                   		'user_id'  => $user_id,
                   		'full_name'     => $full_name,
                   		'level'     => $user_level,
                   		'logged_in' => TRUE
               		);
					$this->session->set_userdata($newdata);
					
					$this->db->where('user_id', $user_id);
					$this->db->update('users', array('user_last_login' => date('Y-m-d H:i:s'), 'user_logins' => $user_logins+1)); 
					audit_log("Successful Login", TRUE, $user_id, $this->input->ip_address());

					redirect('overview');
				}
				else {
					audit_log("Failed Login Attempt - Username: ".$email, FALSE, FALSE, $this->input->ip_address());
					$this->session->set_flashdata('error_message', 'Invalid Email or Password.');
				}


			}

		}

		// Display login page.
		$data['page_title'] = 'Login :: Hydrant Manager';
		$this->load->view('login/view', $data);
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */